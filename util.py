import pickle
from keras.models import model_from_yaml

def load_vectorize_model(NAME_PATH):
    with open(NAME_PATH, 'rb') as f:
        vect = pickle.load(f)
        return vect

# return numpy ndarray
def convert_to_vector(data, vectorize_model):
    vector = vectorize_model.transform(data).toarray()
    return vector

def load_keras_model(MODEL_ARCHITECTURE_PATH, MODEL_WEIGHT_PATH, verbose=0):
    # load YAML and create model
    yaml_file = open(MODEL_ARCHITECTURE_PATH, 'r')
    loaded_model_yaml = yaml_file.read()
    yaml_file.close()
    loaded_model = model_from_yaml(loaded_model_yaml)
    # load weights into new model
    loaded_model.load_weights(MODEL_WEIGHT_PATH)
    if verbose:
        print("Loaded model from disk")
    return loaded_model