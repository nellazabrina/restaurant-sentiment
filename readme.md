# Sentimen Restoran

### Execution Environment
OS: macOS Catalina 10.15.2 <br>
Python 3.7.4 <br>
Packages <br>
 1) pandas==0.25.1
 2) numpy==1.17.2
 3) matplotlib==3.1.1
 4) seaborn==0.9.0
 5) imbalanced-learn==0.6.2
 6) tensorflow==2.2.0
 7) keras==2.3.1
 8) scikit-learn==0.23.1
 9) emoji==0.5.4
10) wordcloud==1.7.0
11) nltk==3.5
12) googletrans==3.0.0
13) gensim==3.8.3
14) barasa https://github.com/neocl/barasa <br>
Anaconda3 4.7.12 <br>
Jupyter Notebook 6.0.1 <br>

### External Source:
1. dict_kbba.json <br>
https://github.com/ramaprakoso/analisis-sentimen/blob/master/kamus/kbba.txt <br>
and modified by me
2. dict_smiley_indonesia.json <br>
https://en.wikipedia.org/wiki/List_of_emoticons <br>
and the meaning is translated into Indonesian word by me